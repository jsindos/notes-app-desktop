import { convertMarkdownToEditorState, convertEditorStateToMarkdown,
  convertMarkdownToEditorStateDifferent, convertEditorStateToMarkdownDifferent,
  convertEditorStateToMarkdownJpuri } from '../../app/actionsAndReducers/editorState'
import { EditorState, convertFromRaw, ContentState, convertToRaw, convertFromHTML } from 'draft-js'

describe('markdown', () => {
  describe('parsing', () => {
    it('links', () => {
      const markdown = 'https://tutorials.botsfloor.com/building-a-facebook-messenger-trivia-bot-with-laravel-part-1-61209b0e35db'
      const editorState = convertMarkdownToEditorState(markdown)
      expect(convertEditorStateToMarkdown(editorState)).toMatchSnapshot()
    });
    it('inline code', () => {
      const markdown = `\`test\`\n`
      const editorState = convertMarkdownToEditorStateDifferent(markdown)
      // console.log(JSON.stringify(editorState.getCurrentContent().getBlockMap(), null, ' '))
      // console.log(convertEditorStateToMarkdownJpuri(editorState))
      expect(convertEditorStateToMarkdownDifferent(editorState)).toEqual(markdown)
    });
    it('code blocks', () => {
      const markdown = `
\`\`\`
console.log('test')
\nconst a = 'b'\n
\`\`\`
      `
      const editorState = convertMarkdownToEditorStateDifferent(markdown)
      // console.log(JSON.stringify(editorState.getCurrentContent().getBlockMap(), null, ' '))
      expect(convertEditorStateToMarkdown(editorState)).toMatchSnapshot()
    });
    it('newlines', () => {
      const markdown = `
Here is a line.

Here is another line.
      `

      const editorState = convertMarkdownToEditorStateDifferent(markdown)
      console.log(convertToRaw(editorState.getCurrentContent()))
    })
  });
});
