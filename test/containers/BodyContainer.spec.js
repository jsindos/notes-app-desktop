import React, { Children } from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'react-router-redux';
import { Markdown } from 'react-showdown'
import KeyCode from 'keycode-js'
import { EditorState, ContentState, convertToRaw } from 'draft-js'

import BodyContainer from '../../app/app/body/BodyContainer'
import { configureStore } from '../../app/store/configureStore';
import { actions as fileActions } from '../../app/actionsAndReducers/file'
import { actions as menuActions } from '../../app/actionsAndReducers/menu'
import { convertMarkdownToEditorState, convertEditorStateToMarkdown } from '../../app/actionsAndReducers/editorState'

Enzyme.configure({ adapter: new Adapter() });

function setup(initialState) {
  const store = configureStore(initialState);
  const history = createBrowserHistory();
  const provider = (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <BodyContainer />
      </ConnectedRouter>
    </Provider>
  );
  const app = mount(provider);
  return {
    store,
    app,
    body: app.find('.main')
  };
}

/*
 * Triggering `fileActions.setAndStoreSelectedFile` then `menuActions.toggleEditMode`
 * does not update `body` to have an `Editor`. Bug with Enzyme.mount or something or some lifecycle method,
 * that updates the DOM.
 * 
 * Instead set an initial state.
 */
const generateInitialState = ({ editModeToggled }) => ({
  selectedFile: { fileName: 'test' },
  menu: { editMode: editModeToggled },
  editorState: [ { fileName: 'test', fileContents: convertMarkdownToEditorState('test') } ]
})

describe('containers', () => {
  describe('BodyContainer', () => {
    it('renders with text', () => {
      const { store, body } = setup(generateInitialState({ editModeToggled: false }))
      expect(body.find(Markdown).text()).toMatch(/^test$/)
    });
    it('accepts writing', () => {
      const { store, body } = setup(generateInitialState({ editModeToggled: true }))
      /**
       * Simulate writing, see
       * https://github.com/facebook/draft-js/issues/325#issuecomment-373212478
       * 
       * As we are inserting a brand new EditorState,
       * store.getState().editorState[0].fileContents.blocks[0].key will have a different key
       * 
       * Note this will stop working if switching to <Editor contentState> over <Editor editorState>
       */
      body.find('DraftEditor').instance().update(EditorState.createWithContent(ContentState.createFromText('a')))
      const markdown = convertEditorStateToMarkdown(store.getState().editorState[0].fileContents)
      expect(markdown).toMatch(/^a$/)
    });
  });
});
