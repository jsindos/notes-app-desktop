import editorStateReducer, { types } from '../../app/actionsAndReducers/editorState'

describe('reducers', () => {
  describe('editorState', () => {
    it('should handle initial state', () => {
      expect(editorStateReducer(undefined, {})).toEqual([])
    });

    it('should store editor state from markdown', () => {
      const action = { type: types.STORE_EDITOR_STATE, fileName: 'test', fileContents: '# 1' }
      expect(editorStateReducer(undefined, action)).toHaveProperty([0, 'fileName'], 'test')
    })
  });
});
