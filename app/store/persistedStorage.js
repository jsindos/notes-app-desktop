const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
import { remote } from 'electron'

import { convertEditorStateArrayToRawState, convertRawStateArrayToEditorState } from '../actionsAndReducers/editorState'

const path = (process.env.NODE_ENV === 'production') ? remote.app.getPath('userData') + '/' : ''

const adapter = new FileSync(path + 'db.json')
const db = low(adapter)

// Set some defaults (required if your JSON file is empty)
db.defaults({ state: '' })
  .write()

export const loadState = () => {
  try {
    const serializedState = db.get('state').value()
    const editorStateArray = convertRawStateArrayToEditorState(JSON.parse(serializedState))
    return {
      editorState: editorStateArray,
      files: editorStateArray.map(e => e.fileName).sort()
    }
    if (serializedState === null) {
      return undefined
    }
  } catch (err) {
    console.error(err)
    return undefined
  }
}

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(convertEditorStateArrayToRawState(state))
    db.update('state', s => serializedState).write()
  } catch (err) {
    // Ignore write errors.
    console.error(err)
  }
}