/**
 * Re-usable withStyles and styled-components styles
 */

export const combineStylesWithProps = (props, stylesArgs) =>
stylesArgs.reduce((prevValue, currentValue) => prevValue(props) + currentValue(props))

export const hoverWithColourWhenNotSelected = props => `
:hover {
  color: ${(props.isSelected || props.notLink) ? null : '#1378b9'};
}
`

export const cursorPointer = props => `
cursor: ${props.notLink ? 'inherit' : 'pointer'};
`

export const ICON = {
  width: '18px',
  height: '18px',
  marginRight: '5px',
  position: 'relative',
  top: '3px'
}

export const CONTEXT_MENU_ICON = Object.assign({}, ICON, { top: '4px' })

export const PATH = '/Users/josephtsindos/Documents/notes/'
export const usePersistedStorage = true
