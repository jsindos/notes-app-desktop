// @flow
import * as React from 'react'
import styled from 'styled-components'

const $Container = styled.div `
height: 100%;
`

type Props = {
  children: React.Node
}

export default class App extends React.Component<Props> {
  props: Props;

  render() {
    return (
      <$Container>
        {this.props.children}
      </$Container>
    )
  }
}
