import { EditorState, convertFromRaw, ContentState, convertToRaw, convertFromHTML } from 'draft-js'
import { mdToDraftjs, draftjsToMd } from 'draftjs-md-converter'
// import { stateToMarkdown } from 'draft-js-export-markdown'
import { stateToMarkdown } from '../../submodules/draft-js-utils/packages/draft-js-export-markdown/src/main'
import { stateFromMarkdown } from '../../submodules/draft-js-utils/packages/draft-js-import-markdown/src/main'
import draftToMarkdown from 'draftjs-to-markdown'
// import { stateToHTML } from 'draft-js-export-html'
// import showdown from 'showdown'
// import TurndownService from 'turndown'

// ACTION TYPES
export const types = {
  STORE_EDITOR_STATE: 'STORE_EDITOR_STATE',
  SET_EDITOR_STATE: 'SET_EDITOR_STATE',
  RENAME_FILE: 'RENAME_FILE',
  DELETE_FILE: 'DELETE_FILE'
}

export const actions = {
  storeEditorState: (fileName, fileContents) => ({ type: types.STORE_EDITOR_STATE, fileName, fileContents }),
  setEditorState: (fileName, editorState) => ({ type: types.SET_EDITOR_STATE, fileName, editorState }),
  renameFile: (fileName, newName) => ({ type: types.RENAME_FILE, fileName, newName }),
  deleteFile: (fileName) => ({ type: types.DELETE_FILE, fileName }),
}

// REDUCERS
const editorStateReducer = createReducer([], {
  [types.STORE_EDITOR_STATE] (state, action) {
    const { fileName, fileContents } = action
    const match = state.find(s => s.fileName === fileName)
    return match ?
    state :
    [ ...state, { fileName: fileName, fileContents: convertMarkdownToEditorStateDifferent(fileContents) } ]
  },
  [types.SET_EDITOR_STATE] (state, action) {
    const { fileName, editorState } = action
    const match = state.find(s => s.fileName === fileName)
    return [ ...state.filter(s => s.fileName !== fileName), Object.assign({}, match, {
      fileContents: editorState
    }) ]
  },
  [types.RENAME_FILE] (state, action) {
    const { fileName, newName } = action
    const match = state.find(s => s.fileName === fileName)
    return [ ...state.filter(s => s.fileName !== fileName), Object.assign({}, match, {
      fileName: newName
    }) ]
  },
  [types.DELETE_FILE] (state, action) {
    const { fileName } = action
    const match = state.find(s => s.fileName === fileName)
    return [ ...state.filter(s => s.fileName !== fileName), Object.assign({}, match, {
      archived: true
    }) ]
  }
})

export default editorStateReducer

// UTILITIES
function createReducer (initialState, handlers) {
  return function reducer (state = initialState, action) {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action)
    } else {
      return state
    }
  }
}

export function convertEditorStateArrayToRawState(editorStateArray) {
  return editorStateArray.map(e => Object.assign({}, e, {
    fileContents: convertToRaw(e.fileContents.getCurrentContent())
  }))
}

export function convertRawStateArrayToEditorState(rawStateArray) {
  return rawStateArray.map(r => Object.assign({}, r, {
    fileContents: EditorState.createWithContent(convertFromRaw(r.fileContents))
  }))
}

export function convertMarkdownToEditorState (fileContents) {
  // Convert using https://github.com/kadikraman/draftjs-md-converter
  // Instead of https://github.com/jpuri/draftjs-to-markdown
  // and https://github.com/jpuri/html-to-draftjs
  // and Showdown
  // const converter = new showdown.Converter()
  /**
   * markdown => RawDraftContentState => ContentState => EditorState
   */
  const rawDraftContentState =  mdToDraftjs(fileContents)
  const contentState = convertFromRaw(rawDraftContentState)
  return EditorState.createWithContent(contentState)
}

/**
 * This function appears to be functioning correctly, after getting the latest pull request.
 * This means Markdown => EditorState conversion is working successfully.
 * The problems previously were with code blocks and inline code, among other things.
 * @param {*} markdown 
 */
export function convertMarkdownToEditorStateDifferent (markdown) {
  const contentState = stateFromMarkdown(markdown)
  return EditorState.createWithContent(contentState)
}

/**
 * This function fails to convert inline code (`a`)
 * but successfully converts code blocks
 * @param {*} editorState 
 */
export function convertEditorStateToMarkdown (editorState) {
  return draftjsToMd(convertToRaw(editorState.getCurrentContent()))
}

/**
 * This function fails to convert code blocks
 * but successfully converts inline code (`a`)
 * 
 * This is demonstrated here https://react-rte.org/demo,
 * which uses stateToMarkdown and stateFromMarkdown under its hood.
 * 
 * https://github.com/sstur/draft-js-import-markdown/issues/1
 * 
 * Note the issue cannot be simulated accurately in react-rte.org/demo, as there is no code block
 * setting in the main editor, to simulate converting it to markdown.
 * @param {*} editorState 
 */
export function convertEditorStateToMarkdownDifferent (editorState) {
  return stateToMarkdown(editorState.getCurrentContent())
}

/**
 * This function fails to convert code blocks
 * but successfully converts inline code (`a`)
 * 
 * @param {*} editorState 
 */
export function convertEditorStateToMarkdownJpuri (editorState) {
  return draftToMarkdown(convertToRaw(editorState.getCurrentContent()))
}
