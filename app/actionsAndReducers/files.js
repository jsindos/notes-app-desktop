import { takeEvery, select, call, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { EditorState } from 'draft-js'

import { PATH } from '../app.global'
import { actions as editorStateActions } from './editorState'
import { actions as fileActions } from './file'
import { actions as filesActions } from './files'
import { actions as contextMenuActions } from './contextMenu'

// ACTION TYPES
export const types = {
  SET_FILES: 'SET_FILES',
  TOGGLE_IS_ADDING_FILE: 'TOGGLE_IS_ADDING_FILE',
  DISABLE_ADD_FILE_BUTTON: 'DISABLE_ADD_FILE_BUTTON',
  ENABLE_ADD_FILE_BUTTON: 'ENABLE_ADD_FILE_BUTTON',
  ADD_FILE: 'ADD_FILE',
  RENAME_FILE: 'RENAME_FILE',
  DELETE_FILE: 'DELETE_FILE',
  // SAGAS
  WORK_ADD_FILE: 'WORK_ADD_FILE',
  WORK_DELETE_FILE: 'WORK_DELETE_FILE',
  WORK_RENAME_FILE: 'WORK_RENAME_FILE',
  FOCUSSED_OUT_OF_INPUT_WORKER: 'FOCUSSED_OUT_OF_INPUT_WORKER',
}

// ACTION CREATORS
export const actions = {
  setFiles: (files) => ({ type: types.SET_FILES, files }),
  toggleIsAddingFile: () => ({ type: types.TOGGLE_IS_ADDING_FILE }),
  disableAddFileButton: () => ({ type: types.DISABLE_ADD_FILE_BUTTON }),
  enableAddFileButton: () => ({ type: types.ENABLE_ADD_FILE_BUTTON }),
  addFile: (fileName) => ({ type: types.ADD_FILE, fileName }),
  deleteFile: (fileName) => ({ type: types.DELETE_FILE, fileName }),
  renameFile: (fileName, newName) => ({ type: types.RENAME_FILE, fileName, newName }),
  // SAGAS
  focussedOutOfInputWorker: () => ({ type: types.FOCUSSED_OUT_OF_INPUT_WORKER }),
  workAddFile: (fileName) => ({ type: types.WORK_ADD_FILE, fileName }),
  workDeleteFile: (fileName) => ({ type: types.WORK_DELETE_FILE, fileName }),
  workRenameFile: (fileName, newName) => ({ type: types.WORK_RENAME_FILE, fileName, newName })
}

// REDUCERS
function filesReducer(state = [], action) {
  const { files, fileName, newName } = action
  switch (action.type) {
    case types.SET_FILES:
      return files
    case types.ADD_FILE:
      return state.concat([ fileName ]).sort()
    case types.DELETE_FILE:
      return state.filter(f => f !== fileName)
    case types.RENAME_FILE:
      return state.filter(f => f !== fileName).concat([ newName ]).sort()
    default:
      return state
  }
}

function newFileReducer(state = { isAddingFile: false, addFileButtonEnabled: true }, action) {
  switch (action.type) {
    case types.TOGGLE_IS_ADDING_FILE:
      return Object.assign({}, state, { isAddingFile: !state.isAddingFile })
    case types.ENABLE_ADD_FILE_BUTTON:
      return Object.assign({}, state, { addFileButtonEnabled: true })
    case types.DISABLE_ADD_FILE_BUTTON:
      return Object.assign({}, state, { addFileButtonEnabled: false })
    default:
      return state
  }
}

// SAGA WORKERS
export function * focussedOutOfInputWorker() {
  yield put(actions.disableAddFileButton())
  yield put(actions.toggleIsAddingFile())
  yield delay(200)
  yield put(actions.enableAddFileButton())
}

export function * workAddFile({ fileName }) {
  yield put(actions.toggleIsAddingFile())
  yield put(actions.addFile(fileName))
  yield put(fileActions.setAndStoreSelectedFile( fileName, '' ))
}

export function * workRenameFile({ fileName, newName }) {
  yield put(actions.renameFile(fileName, newName))
  yield put(contextMenuActions.closeContextMenu())
  // Might also need to reflect this re-naming inside of editorState
}

export function * workDeleteFile({ fileName }) {
  yield put(actions.deleteFile(fileName))
  yield put(filesActions.deleteFile(fileName))
  yield put(contextMenuActions.closeContextMenu())
}

// SAGA WATCHERS
export const sagas = [
  takeEvery(types.FOCUSSED_OUT_OF_INPUT_WORKER, focussedOutOfInputWorker),
  takeEvery(types.WORK_ADD_FILE, workAddFile),
  takeEvery(types.WORK_RENAME_FILE, workRenameFile),
  takeEvery(types.WORK_DELETE_FILE, workDeleteFile),
]

const filesReducers = {
  files: filesReducer,
  newFile: newFileReducer
}

export default filesReducers
