import { sagas as fileSagas } from './file'
import { sagas as filesSagas } from './files'

export default function* rootSaga() {
  yield [
    ...fileSagas,
    ...filesSagas
  ]
}
