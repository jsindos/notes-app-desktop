// ACTION TYPES
export const types = {
  TOGGLE_EDIT_MODE: 'TOGGLE_EDIT_MODE'
}

// ACTION CREATORS
export const actions = {
  toggleEditMode: () => ({ type: types.TOGGLE_EDIT_MODE })
}

// REDUCERS
function menuReducer(state = { editMode: true }, action) {
  switch (action.type) {
    case types.TOGGLE_EDIT_MODE:
      return Object.assign({}, state, { editMode: !state.editMode })
    default:
      return state
  }
}

export default menuReducer
