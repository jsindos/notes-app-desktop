import { actions as filesActions } from './files'
import { PATH } from '../app.global'
import fs from 'fs'

function getFiles() {
  return new Promise(function(resolve, reject) {
    fs.readdir(PATH, (err, data) => {
      err ? reject(err) : resolve(data)
    })
  })
}

export default function initialiseState(dispatch) {
  getFiles().then(response => dispatch(filesActions.setFiles(response)))
}