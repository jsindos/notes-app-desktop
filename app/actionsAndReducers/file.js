import { takeEvery, select, call, put } from 'redux-saga/effects'
import fs from 'fs'

import { PATH } from '../app.global'
import { actions as editorStateActions } from './editorState'

// ACTION TYPES
export const types = {
  SET_SELECTED_FILE: 'SET_SELECTED_FILE',
  // SAGAS
  WORK_GET_AND_SET_FILE: 'WORK_GET_AND_SET_FILE',
  SET_AND_STORE_SELECTED_FILE: 'SET_AND_STORE_SELECTED_FILE'
}

// ACTION CREATORS
export const actions = {
  setSelectedFile: (fileName) => ({ type: types.SET_SELECTED_FILE, fileName }),
  // SAGAS
  workGetAndSetFile: (fileName) => ({ type: types.WORK_GET_AND_SET_FILE, fileName }),
  setAndStoreSelectedFile: (fileName, fileContents) => ({ type: types.SET_AND_STORE_SELECTED_FILE, fileName, fileContents })
}

// REDUCERS
function selectedFileReducer(state = {fileName: null}, action) {
  const { fileName } = action
  switch (action.type) {
    case types.SET_SELECTED_FILE:
      return {
        fileName
      }
    default:
      return state
  }
}

function getFileContents(fileName) {
  return new Promise(function(resolve, reject) {
    fs.readFile(PATH + fileName, 'utf8', (err, data) => {
      err ? reject(err) : resolve(data)
    })
  })
}

// SAGA WORKERS
export function* workGetAndSetFile({fileName}) {
  const exists = yield select(store => store.editorState.find(s => s.fileName === fileName))
  const fileContents = yield !exists && call(getFileContents, fileName)
  yield put(actions.setAndStoreSelectedFile(fileName, fileContents))
}

function* setAndStoreSelectedFile({ fileName, fileContents }) {
  yield put(actions.setSelectedFile(fileName))
  yield put(editorStateActions.storeEditorState(fileName, fileContents))
}

// SAGA WATCHERS
export const sagas = [
  takeEvery(types.SET_AND_STORE_SELECTED_FILE, setAndStoreSelectedFile),
  takeEvery(types.WORK_GET_AND_SET_FILE, workGetAndSetFile)
]

const fileReducers = {
  selectedFile: selectedFileReducer
}

export default fileReducers
