// ACTION TYPES
export const types = {
  OPEN_CONTEXT_MENU: 'OPEN_CONTEXT_MENU',
  CLOSE_CONTEXT_MENU: 'CLOSE_CONTEXT_MENU',
  RENAME: 'RENAME'
}

// ACTION CREATORS
export const actions = {
  openContextMenu: (x, y, fileName) => ({ type: types.OPEN_CONTEXT_MENU, x, y, fileName }),
  closeContextMenu: () => ({ type: types.CLOSE_CONTEXT_MENU }),
  rename: () => ({ type: types.RENAME })
}

// REDUCERS
export default function contextMenuReducer(state = { opened: false, x: 0, y: 0 }, action) {
  const { x, y, fileName } = action
  switch (action.type) {
    case types.OPEN_CONTEXT_MENU:
      return Object.assign({}, state, {
        opened: true,
        x,
        y,
        fileName
      })
    case types.CLOSE_CONTEXT_MENU:
      return Object.assign({}, state, {
        opened: false,
        fileName: undefined,
        renaming: false
      })
    case types.RENAME:
      return Object.assign({}, state, {
        opened: false,
        renaming: true
      })
    default:
      return state
  }
}
