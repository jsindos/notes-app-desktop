// @flow
import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import file from '../actionsAndReducers/file'
import files from '../actionsAndReducers/files'
import menu from '../actionsAndReducers/menu'
import counter from './counter'
import editorState from '../actionsAndReducers/editorState'
import contextMenu from '../actionsAndReducers/contextMenu'

const rootReducer = combineReducers({
  ...file,
  counter,
  menu,
  editorState,
  ...files,
  router,
  contextMenu
})

export default rootReducer
