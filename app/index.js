import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import throttle from 'lodash/throttle'

import 'typeface-roboto'
import './app.global.css'
import Root from './containers/Root'
import { configureStore, history } from './store/configureStore'
import initialiseState from './actionsAndReducers/initialiseState'
import { loadState, saveState } from './store/persistedStorage'
import { usePersistedStorage } from './app.global'

const initialState = usePersistedStorage ? loadState() : undefined
const store = configureStore(initialState)

usePersistedStorage && store.subscribe(throttle(() => {
  saveState(store.getState().editorState)
}), 1000)

!usePersistedStorage && initialiseState(store.dispatch)

render(
  <AppContainer>
    <Root store={store} history={history} />
  </AppContainer>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    const NextRoot = require('./containers/Root'); // eslint-disable-line global-require
    render(
      <AppContainer>
        <NextRoot store={store} history={history} />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}
