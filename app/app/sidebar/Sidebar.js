// @flow
import React, { Component } from 'react'
import styled from 'styled-components'
import { withStyles } from 'material-ui/styles'
import Grid from 'material-ui/Grid'
import FolderOpen from 'material-ui-icons/FolderOpen'
import KeyboardArrowDown from 'material-ui-icons/KeyboardArrowDown'

import ItemContainer from './Item/ItemContainer'
import { $ItemLink } from './Item/Item'
import NewItemContainer from './NewItem/NewItemContainer'
import ToolbarContainer from './Toolbar/ToolbarContainer'
import { ICON, PATH } from '../../app.global'

const $Items = styled.ul `
list-style: none;
margin: 0;
padding: 0;
padding-left: 12px;
`

const styles = () => ({
  container: {
    background: '#fafafa',
    overflowX: 'hidden',
    overflowY: 'auto',
    borderRight: '1px solid #eee'
  },
  icon: ICON
})

type Props = {}

class Sidebar extends Component<Props> {
  props: Props

  render() {
    const { classes, files, isAddingFile } = this.props
    return (
      <Grid
        item
        md={3}
        className={`${classes.container} sidebar`}
        style={{ padding: 0 }}
      >
        <$ItemLink notLink>
          <KeyboardArrowDown className={classes.icon}/>
          <FolderOpen className={classes.icon}/>
          /
          <ToolbarContainer />
        </$ItemLink>
        <$Items>
          {
            isAddingFile && <NewItemContainer />
          }
          {
            files.map((name, i) => <ItemContainer key={i} {...{ name }}/>)
          }
        </$Items>
      </Grid>
    )
  }
}

export default withStyles(styles)(Sidebar)
