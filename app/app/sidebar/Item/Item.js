import React, { Component } from 'react'
import styled from 'styled-components'
import { withStyles } from 'material-ui/styles'
import InsertDriveFile from 'material-ui-icons/InsertDriveFile'

import { ICON, PATH, combineStylesWithProps,
  hoverWithColourWhenNotSelected, cursorPointer } from '../../../app.global'

import InputContainer from '../NewItem/Input/InputContainer'

export const $Item = styled.li `
opacity: ${ props => (props.isAddingFile || props.isRenamingOtherFile) ? '0.3' : 'inherit'};
`

export const $Point = styled.span `
margin-right: 8px;
`

export const $ItemLink = styled.a `
color: ${props => props.isSelected ? '#ffffff' : '#7a7a7a'};
padding: 7px 0 7px 16px;
position: relative;
display: block;
min-height: 25px;
${ props => combineStylesWithProps(props, [
  // hoverWithColourWhenNotSelected,
  cursorPointer
]) }
:hover {
  background: ${props => !(props.isSelected || props.notLink || props.isRightClicked) && '#efefef'};
}
background: ${props => (props.isSelected && props.isRenaming) ? 'rgba(33, 150, 243, 0.7)' :
  props.isSelected ? '#2196F3'
    : props.isRightClicked && '#eaf5fd'};
-webkit-animation-duration: .7s;
animation-duration: .7s;
-webkit-animation-fill-mode: both;
animation-fill-mode: both;
-webkit-transition: all 0.2s;
-o-transition: all 0.2s;
transition: all 0.2s;
-webkit-animation-duration: .2s;
animation-duration: .2s;
-webkit-animation-name: fadeInDown;
animation-name: fadeInDown;
`

const styles = () => ({
  icon: ICON
})

type Props = {}

class Item extends Component<Props> {
  props: Props

  render() {
    const { classes,name, workGetAndSetFile, selectedFile, isAddingFile,
      openContextMenu, contextMenu, closeContextMenu, workRenameFile } = this.props
    const selectedInContextMenu = name === contextMenu.fileName
    const isRenaming = contextMenu.renaming && selectedInContextMenu
    return (
      <$Item
        isAddingFile={isAddingFile}
        isRenamingOtherFile={contextMenu.renaming && !selectedInContextMenu}
        onContextMenu={e => {
          e.preventDefault()
          e.stopPropagation()
          openContextMenu(
            e.clientX || (e.touches && e.touches[0].pageX),
            e.clientY || (e.touches && e.touches[0].pageY),
            name
          )
        }}
      >
        <$ItemLink
          isSelected={ name === selectedFile.fileName }
          isRightClicked={ selectedInContextMenu }
          isRenaming={ isRenaming }
          onClick={ workGetAndSetFile.bind(null, name) }
        >
          <$Point/>
          <InsertDriveFile className={classes.icon}/>
          {
            isRenaming ?
              <InputContainer
                initialInputValue={name}
                onPressEscape={closeContextMenu}
                onPressEnter={workRenameFile.bind(null, name)}
              /> :
              name
          }
        </$ItemLink>
      </$Item>
    )
  }
}

export default withStyles(styles)(Item)
