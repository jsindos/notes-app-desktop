import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { actions as fileActions } from '../../../actionsAndReducers/file'
import { actions as contextMenuActions } from '../../../actionsAndReducers/contextMenu'
import { actions as filesActions } from '../../../actionsAndReducers/files'
import Item from './Item'

function mapStateToProps(state) {
  return {
    selectedFile: state.selectedFile,
    isAddingFile: state.newFile.isAddingFile,
    contextMenu: state.contextMenu
  }
}

function mapDispatchToProps(dispatch) {
  const { workGetAndSetFile } = fileActions
  const { openContextMenu, closeContextMenu } = contextMenuActions
  const { workRenameFile } = filesActions
  return bindActionCreators(
    {
      workGetAndSetFile,
      openContextMenu,
      closeContextMenu,
      workRenameFile
    },
    dispatch
  )
}

class ItemContainer extends Component {
  render() {
    return (
      <Item {...this.props}/>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemContainer)