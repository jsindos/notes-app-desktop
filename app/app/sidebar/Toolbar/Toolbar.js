import React, { Component } from 'react'
import styled from 'styled-components'
import { withStyles } from 'material-ui/styles'

import NoteAdd from 'material-ui-icons/NoteAdd'

import { ICON, combineStylesWithProps,
  hoverWithColourWhenNotSelected, cursorPointer } from '../../../app.global'

const $Toolbar = styled.div`
line-height: 25px;
margin-right: 7px;
float: right;
`

const $Click = styled.span`
${ props => combineStylesWithProps(props, [
  hoverWithColourWhenNotSelected,
  cursorPointer
]) }
`

const styles = () => ({
  icon: ICON
})

class Toolbar extends Component {
  render() {
    const { classes, toggleIsAddingFile, addFileButtonEnabled } = this.props
    return (
      <$Toolbar>
        <$Click onClick={() => addFileButtonEnabled && toggleIsAddingFile()}>
          <NoteAdd className={classes.icon}/>
        </$Click>
      </$Toolbar>
    )
  }
}

export default withStyles(styles)(Toolbar)
