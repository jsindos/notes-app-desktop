import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { actions as filesActions } from '../../../actionsAndReducers/files'
import Toolbar from './Toolbar'

function mapStateToProps(state) {
  return {
    isAddingFile: state.newFile.isAddingFile,
    addFileButtonEnabled: state.newFile.addFileButtonEnabled
  }
}

function mapDispatchToProps(dispatch) {
  const { toggleIsAddingFile } = filesActions
  return bindActionCreators(
    {
      toggleIsAddingFile
    },
    dispatch
  )
}

class ToolbarContainer extends Component {
  render() {
    return (
      <Toolbar {...this.props}/>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToolbarContainer)