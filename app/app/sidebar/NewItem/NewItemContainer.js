import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { actions as filesActions } from '../../../actionsAndReducers/files'
import NewItem from './NewItem'

function mapDispatchToProps(dispatch) {
  const { focussedOutOfInputWorker, toggleIsAddingFile, workAddFile } = filesActions
  return bindActionCreators(
    {
      toggleIsAddingFile,
      workAddFile,
      focussedOutOfInputWorker
    },
    dispatch
  )
}

class NewItemContainer extends Component {
  render() {
    return (
      <NewItem {...this.props}/>
    )
  }
}

export default connect(null, mapDispatchToProps)(NewItemContainer)