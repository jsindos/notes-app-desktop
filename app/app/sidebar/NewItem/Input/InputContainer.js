import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { actions as filesActions } from '../../../../actionsAndReducers/files'
import Input from './Input'

function mapStateToProps(state) {
  return {
    editorState: state.editorState,
  }
}

class InputContainer extends Component {
  render() {
    return (
      <Input {...this.props}/>
    )
  }
}

export default connect(mapStateToProps)(InputContainer)