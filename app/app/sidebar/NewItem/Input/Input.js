import React, { Component } from 'react'
import styled from 'styled-components'

const $Input = styled.input`
font-size: 14px;
width: calc(100% - 72px);
outline-color: ${ props => props.isDuplicate ? 'red' : '-webkit-focus-ring-color' };
outline-style: auto;
outline-width: 1px;
`

const $Error = styled.div`
margin-left: 31px;
border: solid 1px red;
background: rgba(255, 240, 240, 1.0);
padding: 5px 3px;
width: calc(100% - 74px);
color: #7a7a7a;
`

class Input extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputValue: this.props.initialInputValue ? this.props.initialInputValue : ''
    }
  }

  updateInputValue(e) {
    const { editorState, initialInputValue } = this.props
    const value = e.target.value
    const duplicate = editorState.filter(f => (f.fileName === value && f.fileName !== initialInputValue))
    this.setState({
      inputValue: value,
      isDuplicate: Boolean(duplicate.length),
      isArchiveDuplicate: Boolean(duplicate.filter(d => d.archived).length)
    })
  }
  
  handleKeyPress(e) {
    const { isDuplicate } = this.state
    const { onPressEnter, onPressEscape } = this.props
    if (e.key === 'Enter' && !isDuplicate && this.state.inputValue) {
      onPressEnter(this.state.inputValue)
      return
    }
    if (e.keyCode === 27) {
      onPressEscape()
      return
    }
  }

  duplicateErrorMessage() {
    const { inputValue } = this.state
    return (
      <$Error>
        A file or folder <strong>{inputValue}</strong> already exists at this location. Please choose a different name.
      </$Error>
    )
  }

  archivedDuplicateErrorMessage() {
    const { inputValue } = this.state
    return (
      <$Error>
        An archived file or folder <strong>{inputValue}</strong> already exists at this location. Please choose a different name.
      </$Error>
    )
  }

  errorMessage() {
    const { isDuplicate, isArchiveDuplicate } = this.state
    if (isArchiveDuplicate) return this.archivedDuplicateErrorMessage()
    else if (isDuplicate) return this.duplicateErrorMessage()
  }

  render() {
    const { onBlur } = this.props
    const { isDuplicate, inputValue } = this.state
    return (
      <React.Fragment>
        <$Input
          value={inputValue}
          autoFocus
          onBlur={onBlur}
          onKeyDown={this.handleKeyPress.bind(this)}
          onChange={e => this.updateInputValue(e)}
          isDuplicate={isDuplicate}
        />
        {
          this.errorMessage()
        }
      </React.Fragment>
    )
  }
}

export default Input
