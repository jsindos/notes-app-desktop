import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import InsertDriveFile from 'material-ui-icons/InsertDriveFile'

import { ICON } from '../../../app.global'

import InputContainer from './Input/InputContainer'
import { $Item, $Point, $ItemLink } from '../Item/Item'

const styles = () => ({
  icon: ICON
})

class NewItem extends Component {
  render() {
    const { classes, focussedOutOfInputWorker, workAddFile, toggleIsAddingFile } = this.props
    return (
      <$Item>
        <$ItemLink>
          <$Point />
          <InsertDriveFile className={classes.icon}/>
          <InputContainer
            onBlur={focussedOutOfInputWorker}
            onPressEnter={workAddFile}
            onPressEscape={toggleIsAddingFile}
          />
        </$ItemLink>
      </$Item>
    )
  }
}

export default withStyles(styles)(NewItem)
