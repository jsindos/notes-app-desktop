import React, { Component } from 'react'
import { connect } from 'react-redux'

import Sidebar from './Sidebar'

/**
 * Refactor this to a redux selector
 * @param {*} files 
 * @param {*} editorState 
 */
const getUnarchivedFiles = (files, editorState) => 
files.filter(f => !editorState.find(e => e.fileName === f).archived)

function mapStateToProps(state) {
  return {
    files: getUnarchivedFiles(state.files, state.editorState),
    isAddingFile: state.newFile.isAddingFile,
  }
}

class SidebarContainer extends Component {
  render() {
    return (
      <Sidebar {...this.props}/>
    )
  }
}

export default connect(mapStateToProps)(SidebarContainer)