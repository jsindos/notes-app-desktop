// @flow
import React, { Component } from 'react'
import Grid from 'material-ui/Grid'
import styled from 'styled-components'
import { withStyles } from 'material-ui/styles'
import { Markdown } from 'react-showdown'
import { EditorState, convertFromRaw, convertToRaw } from 'draft-js'
import { draftjsToMd } from 'draftjs-md-converter'
import Editor from 'draft-js-plugins-editor'
import createMarkdownPlugin from 'draft-js-markdown-plugin'
import createToolbarPlugin from 'draft-js-static-toolbar-plugin'
import createPrismPlugin from 'draft-js-prism-plugin'
import createLinkifyPlugin from 'draft-js-linkify-plugin'
import { stateToHTML } from 'draft-js-export-html'
import $ from 'jquery'

import Prism from '../../prism/prism'
import { Toolbar, staticToolbarPlugin } from './Toolbar/Toolbar'
import renderLanguageSelect from './RenderLanguageSelect/RenderLanguageSelect'

const $Container = styled.div `
height: 100vh;
display: grid;
grid-template-rows: 6rem 1fr;
grid-template-areas: "toolbar"
                     "editor";
`

const $ToolbarContainer = styled.div`
grid-area: toolbar;
`

const $FixedToolbarContainer = styled.div`
position: fixed;
background: #fff;
border-bottom: 1px solid #ececec;
height: 5em;
width: calc(75% - 90px);
`

const $FixedToolbar = styled.div`
margin-top: 2em;
`

const $EditorContainer = styled.div`
grid-area: editor;
`

function myBlockStyleFn(contentBlock) {
  const type = contentBlock.getType();
  if (type === 'code-block') {
    return 'codeBlock'
  }
  if (type === 'unstyled') {
    return 'unstyled-block'
  }
  if (type === 'unordered-list-item') {
    return 'unordered-list-item-block'
  }
  if (type === 'blockquote') {
    return 'blockquote-block'
  }
  if (type === 'ordered-list-item') {
    return 'ordered-list-item-block'
  }
  if (type === 'header-one') {
    return 'header-one-block'
  }
  if (type === 'header-two') {
    return 'header-two-block'
  }
  if (type === 'header-three') {
    return 'header-three-block'
  }
}

const styleMap = {
  'CODE': {
    padding: '0.2em 0.4em',
    margin: '0',
    fontSize: '85%',
    backgroundColor: 'rgba(27, 31, 35, 0.05)',
    borderRadius: '3px',
    // cursor: 'pointer',
    color: '#c7254e',
    fontFamily: "Menlo, monospace",
    // Pick a CSS property that has no actual impact on your styles, eg. something from SVG.
    strokeDashoffset: '0',
  },
}

const markdownPlugin = createMarkdownPlugin({ renderLanguageSelect })

const plugins = [
  createLinkifyPlugin({ theme: { link: 'linkify' } }),
  markdownPlugin,
  staticToolbarPlugin,
  createPrismPlugin({
    prism: Prism
  })
]

const styles = () => ({
  container: {
    overflowY: 'auto',
    padding: '0 45px 45px 45px !important'
  }
})

class Main extends Component {
  onEditorStateChange(state) {
    const { setEditorState, editorState } = this.props
    setEditorState(editorState.fileName, state)
  }

  componentDidUpdate() {
    const defaultCodeStyles = {
      'padding-right': '0.4em',
      'border-radius': '3px'
    }
    // Reset CODE styles to their default
    $(' [style*="stroke-dashoffset: 0"] ').css(defaultCodeStyles)
    const precedingCodeStyles = {
      'padding-right': '0',
      'border-bottom-right-radius': '0',
      'border-top-right-radius': '0',
    }
    /**
     * Conditional CODE styles, when surrounded by a LINK
     * 
     * Not possible to select tags preceding another tag, so using jQuery
     * https://stackoverflow.com/questions/1132366/selector-for-one-tag-directly-followed-by-another-tag
     * 
     * Succeeding CODE styles can be handled using regular css, so are managed in app.global.css
     * 
     * Note we are using a hack to select the inline CODE blocks
     * https://github.com/facebook/draft-js/issues/957#issuecomment-359076343
     * 
     */
    $( ' .linkify ' ).prev(' [style*="stroke-dashoffset: 0"] ').css(precedingCodeStyles)
    $( ' [style*="stroke-dashoffset: 0"] ' ).prev(' .linkify ').children(' [style*="stroke-dashoffset: 0"] ').css(precedingCodeStyles)
  }

  renderMain() {
    const { selectedFile, editMode, editorState } = this.props
    if (editorState) {
      if (editMode) {
        return (
          <$Container>
            <$EditorContainer>
              <Editor
                editorState={editorState.fileContents}
                onChange={this.onEditorStateChange.bind(this)}
                plugins={plugins}
                blockStyleFn={myBlockStyleFn}
                customStyleMap={styleMap}
              />
            </$EditorContainer>
            <$ToolbarContainer>
              <$FixedToolbarContainer>
                <$FixedToolbar>
                  <Toolbar />
                </$FixedToolbar>
              </$FixedToolbarContainer>
            </$ToolbarContainer>
          </$Container>
        )
      } else {
        return <div dangerouslySetInnerHTML={{__html: stateToHTML(editorState.fileContents.getCurrentContent())}}/>
        // <Markdown markup={draftjsToMd(convertToRaw(editorState.fileContents.getCurrentContent()))} />
      }
    }
  }

  render () {
    const { classes } = this.props
    return (
      <Grid item md={9} className={`${classes.container} main markdown-body`}>
        { this.renderMain() }
      </Grid>
    )
  }
}

export default withStyles(styles)(Main)
