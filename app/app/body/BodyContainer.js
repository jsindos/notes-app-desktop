import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Body from './Body'
import { actions as editorStateActions } from '../../actionsAndReducers/editorState'

function mapStateToProps (state) {
  return {
    selectedFile: state.selectedFile,
    editMode: state.menu.editMode,
    editorState: state.editorState.find(s => s.fileName === state.selectedFile.fileName)
  }
}

function mapDispatchToProps (dispatch) {
  const { setEditorState } = editorStateActions
  return bindActionCreators({setEditorState}, dispatch)
}

class BodyContainer extends Component {
  render () {
    return (
      <Body {...this.props} />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BodyContainer)
