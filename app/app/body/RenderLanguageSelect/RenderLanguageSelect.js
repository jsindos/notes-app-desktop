import React from 'react'
import styled from 'styled-components'

const $SwitcherContainer = styled.div`
position: absolute;
text-align: right;
bottom: -23px;
right: 3px;
`

const $Switcher = styled.div`
display: inline-block;
font-family: sans-serif;
color: #ccc;
letter-spacing: 0.05em;
font-size: 12px;
padding: 0.3em;
cursor: pointer;
position: relative;
`

const $SwitcherSelect = styled.select`
position: absolute;
top: 0;
cursor: pointer;
opacity: 0;
left: 0;
width: 100%;
height: 100%;
`

export default ({
  options,
  onChange,
  selectedValue,
  selectedLabel,
}) => (
  <$SwitcherContainer>
    <$Switcher>
      <$SwitcherSelect
        className={'switcherSelect'}
        value={selectedValue}
        onChange={onChange}
      >
        {options.map(({ label, value }) => (
          <option key={value} value={value}>
            {label}
          </option>
        ))}
      </$SwitcherSelect>
      <div className={'switcherLabel'}>
        {selectedLabel} {String.fromCharCode(9662)}
      </div>
    </$Switcher>
  </$SwitcherContainer>
)