// @flow
import React, { Component } from 'react'
import Grid from 'material-ui/Grid'
import { withStyles } from 'material-ui/styles'

import BodyContainer from './body/BodyContainer'
import SidebarContainer from './sidebar/SidebarContainer'
import BarContainer from './menu/Bar/BarContainer'
import ContextMenuContainer from './menu/ContextMenu/ContextMenuContainer'

const styles = () => ({
  container: {
    height: '100%',
    width: '100%',
    margin: 0
  }
})

type Props = {}

class App extends Component<Props> {
  props: Props

  render() {
    const { classes } = this.props
    return (
      <Grid container className={classes.container}>
        <SidebarContainer/>
        <BodyContainer/>
        {/* <BarContainer/> */}
        <ContextMenuContainer/>
      </Grid>
    )
  }
}

export default withStyles(styles)(App)
