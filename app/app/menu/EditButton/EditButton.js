import React, { Component } from 'react'
import styled from 'styled-components'

const $EditButton = styled.div `
display: flex;
align-items: center;
justify-content: center;
width: 3.5em;
height: 3.5em;
border-radius: 50%;
color: #333;
cursor: pointer;
box-shadow: 0 0 1px 0 #6bd6e6, 0 1px 10px 0 rgba(107, 214, 230, 0.7);
background-color: #fff;
transition: all 0.2s ease-in-out;
font-size: 16px;
`

export default const EditButton = () => {
  return (
    <$EditButton>
    </$EditButton>
  )
}

{/* <div class="userbar-nav">
        <div class="userbar-icon" ng-click="vm.refresh()">
            <i class="glyphicon glyphicon-repeat"></i>
        </div>
    </div> */}