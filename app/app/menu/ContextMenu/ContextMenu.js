import React, { Component } from 'react'
import styled from 'styled-components'
import { withStyles } from 'material-ui/styles'

import Delete from 'material-ui-icons/Delete'
import ModeEdit from 'material-ui-icons/ModeEdit'

import { CONTEXT_MENU_ICON } from '../../../app.global'

const $ContextMenu = styled.div`
-webkit-animation-duration: .7s;
animation-duration: .7s;
-webkit-animation-fill-mode: both;
animation-fill-mode: both;
-webkit-animation-duration: .2s;
animation-duration: .2s;
-webkit-animation-name: fadeIn;
animation-name: fadeIn;
position: absolute;
display: ${props => props.opened ? 'block' : 'none'};
z-index: 9999;
left: ${props => props.left}px;
top: ${props => props.top}px;
`

const $ContextList = styled.ul`
display: block;
position: static;
margin-bottom: 5px;
font-size: 14px;
margin-top: 0;
border: none;
-webkit-box-shadow: 0 1px 4px rgba(0,0,0,0.3);
box-shadow: 0 1px 4px rgba(0,0,0,0.3);
top: 100%;
left: 0;
z-index: 1000;
float: left;
min-width: 160px;
padding: 5px 0;
margin: 2px 0 0;
list-style: none;
text-align: left;
background-color: #ffffff;
-webkit-box-shadow: 0 6px 12px rgba(0,0,0,0.175);
box-shadow: 0 6px 12px rgba(0,0,0,0.175);
-webkit-background-clip: padding-box;
background-clip: padding-box;
margin-top: 0;
margin-bottom: 11.5px;
border-radius: 3px;
`

const $ContextListItem = styled.li`
cursor: pointer;
`

const $ContextListItemDivider = styled.li`
margin: 3px 0;
height: 1px;
overflow: hidden;
background-color: #e5e5e5;
`

const $ContextListLink = styled.a`
padding: 6px 20px;
display: block;
padding: 3px 20px;
clear: both;
font-weight: normal;
line-height: 1.846;
color: #666666;
white-space: nowrap;
-webkit-transition: all 0.2s;
-o-transition: all 0.2s;
transition: all 0.2s;
:hover {
  text-decoration: none;
  color: #141414;
  background-color: #eeeeee;
}
`

const styles = () => ({
  icon: CONTEXT_MENU_ICON
})

class ContextMenu extends Component {
  registerHandlers = () => {
    document.addEventListener('mousedown', this.handleOutsideClick);
    document.addEventListener('touchstart', this.handleOutsideClick);
  }

  handleOutsideClick = (e) => {
    !this.menu.contains(e.target) && this.props.closeContextMenu()
  }

  componentDidMount() {
    this.registerHandlers()
  }

  menuRef = (c) => {
    this.menu = c;
  }

  render() {
    const { opened, x, y, fileName } = this.props.contextMenu
    const { rename, workDeleteFile } = this.props
    return (
      // https://stackoverflow.com/questions/48568753/how-do-i-get-the-wrapped-component-from-proxycomponent?rq=1
      <span ref={this.menuRef}>
        <$ContextMenu opened={opened} left={x} top={y}>
          <$ContextList>
            <$ContextListItem>
              <$ContextListLink onClick={rename}>
                <ModeEdit className={this.props.classes.icon}/>
                Rename
              </$ContextListLink>
            </$ContextListItem>
            <$ContextListItemDivider />
            <$ContextListItem>
              <$ContextListLink onClick={workDeleteFile.bind(null, fileName)}>
                <Delete className={this.props.classes.icon}/>
                Delete
              </$ContextListLink>
            </$ContextListItem>
          </$ContextList>
        </$ContextMenu>
      </span>
    )
  }
}

export default withStyles(styles)(ContextMenu)
