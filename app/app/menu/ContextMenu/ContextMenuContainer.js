import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { actions as contextMenuActions } from '../../../actionsAndReducers/contextMenu'
import { actions as filesActions } from '../../../actionsAndReducers/files'
import ContextMenu from './ContextMenu'

function mapStateToProps(state) {
  return {
    contextMenu: state.contextMenu
  }
}

function mapDispatchToProps(dispatch) {
  const { closeContextMenu, rename } = contextMenuActions
  const { workDeleteFile } = filesActions
  return bindActionCreators(
    {
      closeContextMenu,
      rename,
      workDeleteFile
    },
    dispatch
  )
}

class ContextMenuContainer extends Component {
  render() {
    return (
      <ContextMenu {...this.props}/>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContextMenuContainer)