import React, { Component } from 'react'
import styled from 'styled-components'

const $BarIcon = styled.div `
display: flex;
align-items: center;
justify-content: center;
width: 3.5em;
height: 3.5em;
border-radius: 50%;
color: #333;
cursor: pointer;
box-shadow: 0 0 1px 0 #6bd6e6, 0 1px 10px 0 rgba(107, 214, 230, 0.7);
background-color: #fff;
transition: all 0.05s ease-in-out;
font-size: 16px;
:hover {
  box-shadow: 0 0 1px 0 #6bd6e6, 0 3px 15px 0 rgba(107, 214, 230, 0.95);
}
`

const BarIcon = ({ children }) => {
  return (
    <$BarIcon>
      {children}
    </$BarIcon>
  )
}

export default BarIcon
