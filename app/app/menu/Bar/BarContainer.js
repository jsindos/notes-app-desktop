import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { actions as menuActions } from '../../../actionsAndReducers/menu'

import Bar from './Bar'

function mapStateToProps(state) {
  return {
    // selectedFile: state.selectedFile
  }
}

function mapDispatchToProps(dispatch) {
  const { toggleEditMode } = menuActions
  return bindActionCreators({ toggleEditMode }, dispatch)
}

class BarContainer extends Component {
  render() {
    return (
      <Bar {...this.props}/>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BarContainer)