// @flow
import React, { Component } from 'react'
import styled from 'styled-components'
import ModeEdit from 'material-ui-icons/ModeEdit'
import KeyboardArrowDown from 'material-ui-icons/KeyboardArrowDown'

import BarIcon from '../BarIcon/BarIcon'

const $Bar = styled.div `
bottom: 2.5em;
right: 2.5em;
position: fixed;
z-index: 9999;
font-size: initial !important;
line-height: initial;
margin: 0;
padding: 0;
display: block;
border: 0;
width: auto;
height: auto;
`

const Bar = ({ toggleEditMode }) => {
  return (
    <$Bar onClick={toggleEditMode}>
      <BarIcon>
        <ModeEdit/>
      </BarIcon>
    </$Bar>
  )
}

export default Bar
